import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class NoteService {
  HOST_NOTE_URL = `${this.auth.HOST_BASE_URL}notes/`;

  constructor(private auth: AuthService) {
  }

  getNotes() {
    return this.auth.get(this.HOST_NOTE_URL);
  }

  getNote(id) {
    return this.auth.get(this.HOST_NOTE_URL + id);
  }

  createNote(note) {
    return this.auth.post(this.HOST_NOTE_URL, note);
  }

  deleteNote(id) {
    return this.auth.delete(this.HOST_NOTE_URL + id);
  }
}
