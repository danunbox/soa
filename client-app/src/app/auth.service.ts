import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthService {

  httpOptions: any;

  HOST_BASE_URL = 'http://localhost:3000/api/';

  constructor(private http: HttpClient) {
  }

  login(user, pass) {
    return this.http.post(this.HOST_BASE_URL + 'auth/login', {
      username: user,
      password: pass
    });
  }

  singup(user, pass) {
    return this.http.post(this.HOST_BASE_URL + 'auth/singup', {
      username: user,
      password: pass
    });
  }

  setToken(token) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      })
    };
  }

  isLoggedIn() {
    return !!this.httpOptions;
  }

  get(url) {
    return this.http.get(url, this.httpOptions);
  }

  post(url, body) {
    return this.http.post(url, body, this.httpOptions);
  }

  delete(url) {
    return this.http.delete(url, this.httpOptions);
  }
}
