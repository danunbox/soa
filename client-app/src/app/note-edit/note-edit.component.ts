import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Note } from '../note';
import { NoteService } from '../note.service';

@Component({
  selector: 'app-note-edit',
  templateUrl: './note-edit.component.html',
  styleUrls: ['./note-edit.component.css']
})
export class NoteEditComponent implements OnInit {

  note: Note = new Note();

  @Output()
  add: EventEmitter<Note> = new EventEmitter<Note>();

  constructor(private noteService: NoteService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.noteService.createNote(this.note)
      .subscribe(resp => {
        console.log(resp);
      });
    this.add.emit(this.note);
    this.note = new Note();
  }
}
