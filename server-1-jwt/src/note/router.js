import Router from 'koa-router';
import axios from 'axios';

export const router = new Router();

router.get('/', async (ctx) => {
    const response = ctx.response;

    let notes = await axios.get('http://server-2-db:4000/api/note');
    response.body = notes.data;
    console.log('get all notes: ', response.body);
});

router.get('/:id', async (ctx) => {
    const response = ctx.response;

    let rsp = await axios.get(`http://server-2-db:4000/api/note/${ctx.params.id}`);
    response.body = rsp.data;
    console.log('get note by id: ', response.body);
});

router.post('/', async (ctx) => {
    const response = ctx.response;

    try {
        let resp = await axios.post(`http://server-2-db:4000/api/note/`, ctx.request.body);
        response.body = resp.data;
        response.status = 200;
    } catch (err) {
        response.body = {issue: [{error: err.message}]}
    }
});

router.del('/:id', async (ctx) => {
    const response = ctx.response;

    await axios.delete(`http://server-2-db:4000/api/note/${ctx.params.id}`);
    ctx.response.status = 204; // no content
    console.log('delete note by id: ', response.body);
});