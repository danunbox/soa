import Router from 'koa-router';
import userStore from '../auth/store'
import noteStore from "../note/store";

export const router = new Router();

router.get('/', async (ctx) => {
    const response = ctx.response;
    response.body = await userStore.find({});
    console.log('get all users: ', response.body);
});

router.get('/:id', async (ctx) => {
    const response = ctx.response;
    response.body = await userStore.findOne({_id: ctx.params.id});
    console.log('get USER by id: ', response.body);
});

router.del('/:id', async (ctx) => {
    await noteStore.remove({ _id: ctx.params.id });
    ctx.response.status = 204; // no content
});