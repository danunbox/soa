import Koa from 'koa';
import Router from 'koa-router';
import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser'
import {exceptionHandler, timingLogger} from "./utils/middlewares";
import {router as noteRouter} from './note';
import {router as authRouter} from './auth';
import jwt from 'koa-jwt';

const app = new Koa();
app.use(cors());
app.use(exceptionHandler);
app.use(timingLogger);
app.use(bodyParser());

const prefix = '/api';
//public api
const publicApiRouter = new Router({prefix});
publicApiRouter
    .use('/auth', authRouter.routes());
app
    .use(publicApiRouter.routes())
    .use(publicApiRouter.allowedMethods());

app.use(jwt({secret: 'my-secret'}));

//protected api
const protectedApiRouter = new Router({prefix});
protectedApiRouter
    .use('/notes', noteRouter.routes())
app
    .use(protectedApiRouter.routes())
    .use(protectedApiRouter.allowedMethods());

app.listen(3000);
console.log('---> started on port 3000');



// app.use(async ctx => {
//     ctx.body = 'Hello Dio!';
//     console.log('started on port 3000');
// });
// app.listen(3000);
// console.log('---> started on port 3000')