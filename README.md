#install
cd client-app => npm i
cd server-1-jwt => npm i
cd server-2-db => npm i

#run
cd client-app => ng serve
cd server-1-jwt => npm run dev
cd server-2-db => npm run dev

#flow
client-app communicates with server-1-jwt to get a token and notes
server-1-jwt communicates with server-2-db to get notes

#steps
1.login to get a token(POST http://localhost:3000/api/auth/signup->{username: 'xxx', password: 'xxx'}) - this will return token
1.rest operations(make sure you use header Authorization: Bearer ${TOKEN_XXX}) on http://localhost:3000/api/notes