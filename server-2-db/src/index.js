import Koa from 'koa';
import Router from 'koa-router';
import bodyParser from "koa-bodyparser";
import { timingLogger, exceptionHandler, jwtConfig } from './utils';
import { router as noteRouter } from './note';
import cors from '@koa/cors';

const app = new Koa();

app.use(cors());
app.use(exceptionHandler);
app.use(timingLogger);
app.use(bodyParser());

const prefix = '/api';

// public
const publicApiRouter = new Router({ prefix });
publicApiRouter
  .use('/note', noteRouter.routes());
app
  .use(publicApiRouter.routes())
  .use(publicApiRouter.allowedMethods());

if (!module.parent) {
  app.listen(4000);
  console.log('started on port 4000');
}
