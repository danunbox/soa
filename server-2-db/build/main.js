require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("koa-router");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(2);


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {

var _koa = __webpack_require__(4);

var _koa2 = _interopRequireDefault(_koa);

var _koaRouter = __webpack_require__(0);

var _koaRouter2 = _interopRequireDefault(_koaRouter);

var _koaBodyparser = __webpack_require__(5);

var _koaBodyparser2 = _interopRequireDefault(_koaBodyparser);

var _utils = __webpack_require__(6);

var _note = __webpack_require__(9);

var _cors = __webpack_require__(13);

var _cors2 = _interopRequireDefault(_cors);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = new _koa2.default();

app.use((0, _cors2.default)());
app.use(_utils.exceptionHandler);
app.use(_utils.timingLogger);
app.use((0, _koaBodyparser2.default)());

var prefix = '/api';

// public
var publicApiRouter = new _koaRouter2.default({ prefix: prefix });
publicApiRouter.use('/note', _note.router.routes());
app.use(publicApiRouter.routes()).use(publicApiRouter.allowedMethods());

if (!module.parent) {
  app.listen(4000);
  console.log('started on port 4000');
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)(module)))

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("koa");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("koa-bodyparser");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _middlewares = __webpack_require__(7);

Object.keys(_middlewares).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _middlewares[key];
    }
  });
});

var _constants = __webpack_require__(8);

Object.keys(_constants).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _constants[key];
    }
  });
});

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var exceptionHandler = exports.exceptionHandler = async function exceptionHandler(ctx, next) {
  try {
    return await next();
  } catch (err) {
    ctx.body = { message: err.message || 'Unexpected error.' };
    ctx.status = err.status || 500;
  }
};

var timingLogger = exports.timingLogger = async function timingLogger(ctx, next) {
  var start = Date.now();
  await next();
  console.log(ctx.method + ' ' + ctx.url + ' => ' + ctx.response.status + ', ' + (Date.now() - start) + 'ms');
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var jwtConfig = exports.jwtConfig = { secret: 'my-secret' };

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _router = __webpack_require__(10);

Object.keys(_router).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _router[key];
    }
  });
});

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.router = undefined;

var _koaRouter = __webpack_require__(0);

var _koaRouter2 = _interopRequireDefault(_koaRouter);

var _store = __webpack_require__(11);

var _store2 = _interopRequireDefault(_store);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = exports.router = new _koaRouter2.default();

router.get('/', async function (ctx) {
  var response = ctx.response;
  response.body = await _store2.default.find({});
  response.status = 200; // ok
});

router.get('/:id', async function (ctx) {
  var note = await _store2.default.findOne({ _id: ctx.params.id });
  var response = ctx.response;
  if (note) {
    response.body = note;
    response.status = 200; // ok
  } else {
    response.status = 404; // not found
  }
});

var createNote = async function createNote(note, response) {
  try {
    response.body = await _store2.default.insert(note);
    response.status = 201; // created
  } catch (err) {
    response.body = { issue: [{ error: err.message }] };
    response.status = 400; // bad request
  }
};

router.post('/', async function (ctx) {
  return await createNote(ctx.request.body, ctx.response);
});

router.put('/:id', async function (ctx) {
  var note = ctx.request.body;
  var id = ctx.params.id;
  var noteId = note._id;
  var response = ctx.response;
  if (noteId && noteId !== id) {
    response.body = { issue: [{ error: 'Param id and body _id should be the same' }] };
    response.status = 400; // bad request
    return;
  }
  if (!noteId) {
    await createNote(note, response);
  } else {
    var updatedCount = await _store2.default.update({ _id: id }, note);
    if (updatedCount === 1) {
      response.body = note;
      response.status = 200; // ok
    } else {
      response.body = { issue: [{ error: 'Resource no longer exists' }] };
      response.status = 405; // method not allowed
    }
  }
});

router.del('/:id', async function (ctx) {
  await _store2.default.remove({ _id: ctx.params.id });
  ctx.response.status = 204; // no content
});

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NoteStore = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _nedbPromise = __webpack_require__(12);

var _nedbPromise2 = _interopRequireDefault(_nedbPromise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var NoteStore = exports.NoteStore = function () {
  function NoteStore(_ref) {
    var filename = _ref.filename,
        autoload = _ref.autoload;

    _classCallCheck(this, NoteStore);

    this.store = (0, _nedbPromise2.default)({ filename: filename, autoload: autoload });
  }

  _createClass(NoteStore, [{
    key: 'find',
    value: async function find(props) {
      return this.store.find(props);
    }
  }, {
    key: 'findOne',
    value: async function findOne(props) {
      return this.store.findOne(props);
    }
  }, {
    key: 'insert',
    value: async function insert(note) {
      var noteText = note.text;
      if (!noteText) {
        // validation
        throw new Error('Missing text property');
      }
      return this.store.insert(note);
    }
  }, {
    key: 'update',
    value: async function update(props, note) {
      return this.store.update(props, note);
    }
  }, {
    key: 'remove',
    value: async function remove(props) {
      return this.store.remove(props);
    }
  }]);

  return NoteStore;
}();

exports.default = new NoteStore({ filename: './db/notes.json', autoload: true });

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("nedb-promise");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("@koa/cors");

/***/ })
/******/ ]);
//# sourceMappingURL=main.map